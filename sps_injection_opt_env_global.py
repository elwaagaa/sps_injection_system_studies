"""
Global optimisation environment for all optics, which initiates and optimises the low-level environments 
"""

import numpy as np
import pandas as pd
from scipy import optimize
import matplotlib.pyplot as plt

#Import accelerator physics library with functions
import acc_phy_lib_elias 

#Import high-level environment
import sps_injection_opt_env_low_level
import sps_injection_opt_env_low_level_fixed_magnets


class OptEnv_global():
    
    def __init__(self):
        self.msi_blade_dx = 0.02  #thickness of second half of MSI blade, if new MSI blade is used  
        self.l_mkps_mod = 0.599 #length in meters of single MKP-S module
        self.solver = 'Nelder-Mead'
        self.second_solver = 'Powell' #to optimise MSI and MKP kick as alternative, when fewer parameters are involved 

        
    #Global optimisation stepping function, where the n-vector contains the numnber of modules and number of tanks 
    def step(self, n):
        
        print("\n------------- Optimising for {} MKP modules in {} tanks -------------\n".format(n[0], n[1]))
    
        #Fill in the string value for one or several modules, for naming of files 
        if n[1] == 1:
            self.mkp_str = 'single_module'
        elif n[1] == 2:
            self.mkp_str = 'double_module'
        elif n[1] == 3:
            self.mkp_str = 'triple_module'
        else:
            self.mkp_str = '{}_module'.format(n[1])
    
        #Make sure number of modules also are positive
        if (n[0] <= 0) or (n[1] <= 0):
            objective_func = 1 + n[1]**2 + n[0]**2
        else:   
            #Load the low-level environments
            self.env_q20 = sps_injection_opt_env_low_level.OptEnv('q20', n[0], n[1])
            self.env_q26 = sps_injection_opt_env_low_level.OptEnv('q26', n[0], n[1])
            self.env_sftpro = sps_injection_opt_env_low_level.OptEnv('sftpro', n[0], n[1])
                     
            # ---------------------------- Set initial and boundary conditions --------------------------
            #Extract original values before optimising 
            mkp_org_kick = self.env_sftpro.madx.globals['kmkpa11931'] 
            mkp_org_l = self.env_sftpro.madx.sequence['short_injection_basic'].elements['mkp_module_1'].l #same length for all modules
            kmsi_original_kick = self.env_sftpro.madx.globals['kmsi']
            self.BRHO = self.env_sftpro.madx.sequence['short_injection_basic'].beam.pc*3.3356
            self.Bdl_tot_q20 = 3.17200000e-01  #Original total Bdl values for all MKPs, calculated from notebook "today_sps_injection_acceptance_q20"
            self.Bdl_tot_q26 = 3.17200000e-01
            self.Bdl_tot_sftpro = 2.14273280e-01
            
            #Minimum distance between MKPs
            self.s_min_mkp = mkp_org_l/2*n[0]/5 + \
                mkp_org_l/2*n[0]/5 + 0.6 #at least 2*300 mm between tanks such that vacuum valves can fit 
            
            # ------------------- Load the initial conditions -----------------------------------
            #Starting initial conditions, if no previous ICs exist
            self.x0 = np.zeros(4) 
            self.x0[0] = kmsi_original_kick   #original kmsi
            self.x0[1] = 1.4*mkp_org_kick   #original MKP kick   #for one MKP tank and low n, set about 1.5
            self.x0[2] = 33.1      #MSI_1 first position
            self.x0[3] = 64.0      #MKP tank 1 position 
            for k in range(1, n[1]): #add initial conditions for all additional MKP tanks
                self.x0 = np.append(self.x0, self.env_sftpro.s_min_mkp+0.05)  #MKP tank k w.r.t tank k-1
        
            #Load previous initial conditions, if they exist 
            try:
                x0_q20 = np.genfromtxt("IC_data/{}/{}_n{}_{}_optimiser_results.csv".format('q20', self.mkp_str, n[0], 'q20')) 
                print("Q20: Loading initial conditions from previous runs...")
            except OSError:
                x0_q20 = self.x0
            try:
                x0_q26 = np.genfromtxt("IC_data/{}/{}_n{}_{}_optimiser_results.csv".format('q26', self.mkp_str, n[0], 'q26')) 
                print("Q26: Loading initial conditions from previous runs...")
            except OSError:
                x0_q26 = self.x0
            try:
                x0_sftpro = np.genfromtxt("IC_data/{}/{}_n{}_{}_optimiser_results.csv".format('sftpro', self.mkp_str, n[0], 'sftpro')) #print("Loading initial conditions from previous runs...")
                print("SFTPRO: Loading initial conditions from previous runs...\n")
            except OSError:   
                x0_sftpro = self.x0
            
            #Bounds for optimisation, to minimize the number of time MADX crashes 
            rel_diff = 1.5*abs(kmsi_original_kick)  #+/-150% kick for MSI if needed 
            kmsi_min = kmsi_original_kick - rel_diff
            kmsi_max = kmsi_original_kick + rel_diff
            bound1 = (kmsi_min, kmsi_max)  #kmsi
            bound2 = (0, 15*mkp_org_kick)     #MKP kick min and max, has to be positive
            bound3 = (33.35, 56.0)      #MSI_1 first position
            bound4 = (self.env_q20.first_mkp_min_s, 82.5)      #MKP tank 1 position
            bounds_mkp = (self.env_sftpro.s_min_mkp, 1.1*self.env_sftpro.s_min_mkp)      #MKP tank i position w.r.t MKP tank i-1 
            self.bounds = [bound1, bound2, bound3, bound4]
            for k in range(1, n[1]):
               self.bounds.append(bounds_mkp)  #MKP tank k w.r.t tank k-1

            #Optimize the values for Q20 and Q26, as SFTPRO will always have lower Bdl and can more easily be optimised for
            print("Optimising for Q20 optics...")
            self.res_q20 = optimize.minimize(self.env_q20.step, x0_q20, method=self.solver, bounds=self.bounds, options={'adaptive': True})
            print("Optimising for Q26 optics...")
            self.res_q26 = optimize.minimize(self.env_q26.step, x0_q26, method=self.solver, bounds=self.bounds, options={'adaptive': True})
    
            #Save the optimisation results as initial conditions for future runs
            pd.DataFrame(self.res_q20.x).to_csv("IC_data/{}/{}_n{}_{}_optimiser_results.csv".format('q20', self.mkp_str, n[0], 'q20'), index=False, header=False)
            pd.DataFrame(self.res_q26.x).to_csv("IC_data/{}/{}_n{}_{}_optimiser_results.csv".format('q26', self.mkp_str, n[0], 'q26'), index=False, header=False)
    
            #Find the integrated field of all optimisations, and the specific B-field for each 
            self.BRHO_q20 = self.env_q20.madx.sequence['short_injection_basic'].beam.pc*3.3356
            self.BRHO_q26 = self.env_q26.madx.sequence['short_injection_basic'].beam.pc*3.3356
            self.BRHO_sftpro = self.env_sftpro.madx.sequence['short_injection_basic'].beam.pc*3.3356
            self.Bdl_tot_q20 =  (n[1]*self.res_q20.x[1])*self.BRHO_q20 #(Sum all kicks)*BRHO --> mkp_nr of equal MKPs
            self.Bdl_tot_q26 =  (n[1]*self.res_q26.x[1])*self.BRHO_q26
            self.Bdl_tot_sftpro = 0.2  #(n[1]*self.res_sftpro.x[1])*self.BRHO_sftpro  #sftpro always lower, to save time in optimisation can set it to average value and then optimise at the end 
            self.B_q20 = self.Bdl_tot_q20/(self.l_mkps_mod*n[0]*n[1])
            self.B_q26 = self.Bdl_tot_q26/(self.l_mkps_mod*n[0]*n[1])
            self.B_sftpro = self.Bdl_tot_sftpro/(self.l_mkps_mod*n[0]*n[1])
            Bdls = [self.Bdl_tot_q20, self.Bdl_tot_q26, self.Bdl_tot_sftpro]
            Bs = [self.B_q20, self.B_q26, self.B_sftpro]
            
            #Now fix the magnet positions and lengths of the optics with the highest Bdl, and impose on other optics
            self.max_Bdl = max(Bdls)
            max_index = Bdls.index(self.max_Bdl)  
            self.B = Bs[max_index] #attach corresponding field of case with higest Bdl
            if max_index == 0:
                res = self.res_q20.x
                self.Bdl_max_type = 'q20'
                self.lower_Bdl_optics_list = ['q26', 'sftpro']
            elif max_index == 1:
                res = self.res_q26.x
                self.Bdl_max_type = 'q26'
                self.lower_Bdl_optics_list = ['q20', 'sftpro']
            elif max_index == 2:
                res = self.res_sftpro.x
                self.Bdl_max_type = 'sftpro'      
                self.lower_Bdl_optics_list = ['q20', 'q26']
            print("\nBdl highest for {}\n".format(self.Bdl_max_type))
        
            #load all values from optimisation results with highest Bdl, except the MSI and MKP strengths which remain free parameters 
            self.y0 = res[2:]
            
            #Check that acceptance for SFTPRO is not exceeded 
            print("Fixing magnet positions and lengths for SFTPRO optics, testing envelope for dumped beam...")
            self.x0_sftpro_new  = x0_sftpro[:2]  #from SFTPRO initial conditions, for next step in optimisation 
            self.env_sftpro = sps_injection_opt_env_low_level_fixed_magnets.OptEnv('sftpro', n[0], n[1])
            self.res_sftpro = optimize.minimize(self.env_sftpro.step, self.x0_sftpro_new, args=(self.y0), method=self.second_solver) 
            self.x0_sftpro_new = self.res_sftpro.x[:2] #store these results for kick optimisation later 
            pen_sftpro = self.res_sftpro.fun  #SFTPRO has widest envelope, add its penalty function value into final objective function
            
            #Define objective function
            objective_func = self.max_Bdl**2 + self.B**2 + pen_sftpro**2
            
            print("\nGlobal objective function of step method = {:.3f}\n".format(objective_func))
            
        return objective_func            
            
            
    # -------------------------- Optimise MSI and MKP kicks for optics with lower Bdl ------------------------------
    def optimise_kicks(self, n):    
        
        print("\nBdl highest for {}, re-optimise for other optics...\n".format(self.Bdl_max_type))
        
        #For optics with lower Bdl, create optimisation environments and append them to list 
        for ele in self.lower_Bdl_optics_list:
            if ele == 'q20':
                print("Fixing magnet positions and lengths for Q20 optics...")
                self.env_q20 = sps_injection_opt_env_low_level_fixed_magnets.OptEnv('q20', n[0], n[1], self.max_Bdl)
                self.env_q20.n = n[0] 
                self.x0_q20_new = self.res_q20.x[:2]  #IC for MSI and MKP kicks: results from previous optmisation
            if ele == 'q26':
                print("Fixing magnet positions and lengths for Q26 optics...")
                self.env_q26 = sps_injection_opt_env_low_level_fixed_magnets.OptEnv('q26', n[0], n[1], self.max_Bdl)
                self.env_q26.n = n[0] 
                self.x0_q26_new = self.res_q26.x[:2]
            if ele == 'sftpro':
                print("Fixing magnet positions and lengths for SFTPRO optics...")
                self.env_sftpro = sps_injection_opt_env_low_level_fixed_magnets.OptEnv('sftpro', n[0], n[1], self.max_Bdl)
                self.env_sftpro.n = n[0]    
                #self.x0_sftpro_new[1] = 0.9*self.res_sftpro.x[1]

        #Iterate and optimize the MSI/MKP kick with fixed magnet lengths and positions from case with highest Bdl
        for ele in self.lower_Bdl_optics_list:
            if ele == 'q20':
                print("\nOptimising for Q20...")
                self.res_q20 = optimize.minimize(self.env_q20.step, self.x0_q20_new, args=(self.y0), method=self.second_solver, options={'disp': True}) 
                print("Objective function value: {:.3e}\n".format(self.res_q20.fun))
            if ele == 'q26':
                print("\nOptimising for Q26...\n")
                self.res_q26 = optimize.minimize(self.env_q26.step, self.x0_q26_new, args=(self.y0), method=self.second_solver, options={'disp': True}) 
                print("Objective function value: {:.3e}\n".format(self.res_q26.fun))
            if ele == 'sftpro':
                print("\nOptimising for SFTPRO...\n")
                self.res_sftpro = optimize.minimize(self.env_sftpro.step, self.x0_sftpro_new, args=(self.y0), method=self.second_solver, options={'disp': True}) 
                print("Objective function value: {:.3e}\n".format(self.res_sftpro.fun))
        
        #Find the beam envelope for normal and dumped beam
        self.twiss_q20, self.twiss_no_mkp_q20, self.A_x_min_tot_q20, self.s_min_tot_q20, self.A_y_min_tot_q20, self.s_y_min_tot_q20 = self.env_q20.return_twisses()
        self.twiss_q26, self.twiss_no_mkp_q26, self.A_x_min_tot_q26, self.s_min_tot_q26, self.A_y_min_tot_q26, self.s_y_min_tot_q26 = self.env_q26.return_twisses()
        self.twiss_sftpro, self.twiss_no_mkp_sftpro, self.A_x_min_tot_sftpro, self.s_min_tot_sftpro, self.A_y_min_tot_sftpro, self.s_y_min_tot_sftpro = self.env_sftpro.return_twisses()
        print("Final beam positions:")
        print("Q20: x = {:.2e} m, p_x = {:.2e} GeV/c".format(self.twiss_q20.x['qf.12010'], self.twiss_q20.px['qf.12010']))
        print("Q26: x = {:.2e} m, p_x = {:.2e} GeV/c".format(self.twiss_q26.x['qf.12010'], self.twiss_q26.px['qf.12010']))
        print("SFTPRO: x = {:.2e} m, p_x = {:.2e} GeV/c".format(self.twiss_sftpro.x['qf.12010'], self.twiss_sftpro.px['qf.12010']))
        
        #Get neat aperture for SPS, and select index for the desired apertures
        self.new_pos_x_q20, self.aper_neat_x_q20 = acc_phy_lib_elias.get_apertures_real(self.twiss_q20)
        self.ind_q20 = (self.new_pos_x_q20 > self.twiss_q20.loc['msi_test_4'].s)
        self.offset_x_q20 = acc_phy_lib_elias.get_aper_offset_real(self.twiss_q20)
        self.new_pos_x_q26, self.aper_neat_x_q26 = acc_phy_lib_elias.get_apertures_real(self.twiss_q26)
        self.ind_q26 = (self.new_pos_x_q26 > self.twiss_q26.loc['msi_test_4'].s)
        self.offset_x_q26 = acc_phy_lib_elias.get_aper_offset_real(self.twiss_q26)
        self.new_pos_x_sftpro, self.aper_neat_x_sftpro = acc_phy_lib_elias.get_apertures_real(self.twiss_sftpro)
        self.ind_sftpro = (self.new_pos_x_sftpro > self.twiss_sftpro.loc['msi_test_4'].s)
        self.offset_x_sftpro = acc_phy_lib_elias.get_aper_offset_real(self.twiss_sftpro)    


    #Function to plot Q20 optics beam envelope
    def plot_injection_q20(self, fig):
        fig.suptitle('SPS injection - Q20',fontsize=20)
        ax = fig.add_subplot(1, 1, 1)  # create an axes object in the figure
        ax.yaxis.label.set_size(20)
        ax.xaxis.label.set_size(20)
        plt.xticks(fontsize=14)  
        plt.yticks(fontsize=14)
        plt.ylim(-0.1, 0.15)
        plt.xlim(30, 92)
        acc_phy_lib_elias.plot_envelope(self.twiss_q20, self.env_q20.sige, self.env_q20.ex, self.env_q20.ey, ax)
        if self.env_q20.default_blade_flag == 0:
            acc_phy_lib_elias.plot_msi_blade(self.twiss_q20, 'msi_test_1', 'msi_test_4', ax, dx=self.msi_blade_dx)
        else:
            acc_phy_lib_elias.plot_default_msi_blade(self.twiss_q20, ax, 'msi_test_1')
        acc_phy_lib_elias.plot_aper_real(self.new_pos_x_q20[self.ind_q20], self.aper_neat_x_q20[self.ind_q20], ax, offset=self.offset_x_q20[self.ind_q20])
        plt.text(31.5, 0.03, '$A_{{x, min}}= {:.2f}$'.format(self.A_x_min_tot_q20), fontsize=15, horizontalalignment='left')
        plt.text(31.5, 0.015, 'at $s = {:.1f}$ m'.format(self.s_min_tot_q20), fontsize=15, horizontalalignment='left')
        plt.text(31.5, -0.05, '$\Delta Bdl = {:>.1f}\%$'.format(100*(self.Bdl_tot_q20 - self.env_q20.Bdl_org)/self.env_q20.Bdl_org), fontsize=15, horizontalalignment='left')
        plt.text(31.5, -0.065, '$B = {:>.4f}$ T'.format(self.B_q20), fontsize=15, horizontalalignment='left')
        plt.text(31.5, -0.09, 'n = {}'.format(self.env_q20.n), fontsize=15, horizontalalignment='left')
        
        
    #Function to plot Q26 optics beam envelope
    def plot_injection_q26(self, fig):
        fig.suptitle('SPS injection - Q26',fontsize=20)
        ax = fig.add_subplot(1, 1, 1)  # create an axes object in the figure
        ax.yaxis.label.set_size(20)
        ax.xaxis.label.set_size(20)
        plt.xticks(fontsize=14)  
        plt.yticks(fontsize=14)
        plt.ylim(-0.1, 0.15)
        plt.xlim(30, 92)
        acc_phy_lib_elias.plot_envelope(self.twiss_q26, self.env_q26.sige, self.env_q26.ex, self.env_q26.ey, ax)
        if self.env_q26.default_blade_flag == 0:
            acc_phy_lib_elias.plot_msi_blade(self.twiss_q26, 'msi_test_1', 'msi_test_4', ax, dx=self.msi_blade_dx)
        else:
            acc_phy_lib_elias.plot_default_msi_blade(self.twiss_q26, ax, 'msi_test_1')
        acc_phy_lib_elias.plot_aper_real(self.new_pos_x_q26[self.ind_q26], self.aper_neat_x_q26[self.ind_q26], ax, offset=self.offset_x_q26[self.ind_q26])
        plt.text(31.5, 0.03, '$A_{{x, min}}= {:.2f}$'.format(self.A_x_min_tot_q26), fontsize=15, horizontalalignment='left')
        plt.text(31.5, 0.015, 'at $s = {:.1f}$ m'.format(self.s_min_tot_q26), fontsize=15, horizontalalignment='left')
        plt.text(31.5, -0.05, '$\Delta Bdl = {:>.1f}\%$'.format(100*(self.Bdl_tot_q26 - self.env_q20.Bdl_org)/self.env_q26.Bdl_org), fontsize=15, horizontalalignment='left')
        plt.text(31.5, -0.065, '$B = {:>.4f}$ T'.format(self.B_q26), fontsize=15, horizontalalignment='left')
        plt.text(31.5, -0.09, 'n = {}'.format(self.env_q26.n), fontsize=15, horizontalalignment='left')


    #Function to plot SFTPRO optics beam envelope
    def plot_injection_sftpro(self, fig):
        fig.suptitle('SPS injection - SFTPRO',fontsize=20)
        ax = fig.add_subplot(1, 1, 1)  # create an axes object in the figure
        ax.yaxis.label.set_size(20)
        ax.xaxis.label.set_size(20)
        plt.xticks(fontsize=14)  
        plt.yticks(fontsize=14)
        plt.ylim(-0.1, 0.15)
        plt.xlim(30, 92)
        acc_phy_lib_elias.plot_envelope(self.twiss_sftpro, self.env_sftpro.sige, self.env_sftpro.ex, self.env_sftpro.ey, ax, nx=6, ny=4)
        if self.env_sftpro.default_blade_flag == 0:
            acc_phy_lib_elias.plot_msi_blade(self.twiss_sftpro, 'msi_test_1', 'msi_test_4', ax, dx=self.msi_blade_dx)
        else:
            acc_phy_lib_elias.plot_default_msi_blade(self.twiss_sftpro, ax, 'msi_test_1')
        acc_phy_lib_elias.plot_aper_real(self.new_pos_x_sftpro[self.ind_sftpro], self.aper_neat_x_sftpro[self.ind_sftpro], ax, offset=self.offset_x_sftpro[self.ind_sftpro])
        plt.text(31.5, 0.03, '$A_{{x, min}}= {:.2f}$'.format(self.A_x_min_tot_sftpro), fontsize=15, horizontalalignment='left')
        plt.text(31.5, 0.015, 'at $s = {:.1f}$ m'.format(self.s_min_tot_sftpro), fontsize=15, horizontalalignment='left')
        plt.text(31.5, -0.05, '$\Delta Bdl = {:>.1f}\%$'.format(100*(self.Bdl_tot_sftpro - self.env_sftpro.Bdl_org_sftpro)/self.env_sftpro.Bdl_org_sftpro), fontsize=15, horizontalalignment='left')
        plt.text(31.5, -0.065, '$B = {:>.4f}$ T'.format(self.B_sftpro), fontsize=15, horizontalalignment='left')
        plt.text(31.5, -0.09, 'n = {}'.format(self.env_sftpro.n), fontsize=15, horizontalalignment='left')


    #Function to plot Q20 optics beam envelope for dumped beam
    def plot_injection_q20_no_mkp(self, fig):
        fig.suptitle('SPS injection - Q20, dumped beam',fontsize=20)
        ax = fig.add_subplot(1, 1, 1)  # create an axes object in the figure
        ax.yaxis.label.set_size(20)
        ax.xaxis.label.set_size(20)
        plt.xticks(fontsize=14) 
        plt.yticks(fontsize=14)
        plt.ylim(-0.1, 0.15)
        plt.xlim(30, 92)
        acc_phy_lib_elias.plot_envelope(self.twiss_no_mkp_q20, self.env_q20.sige, self.env_q20.ex, self.env_q20.ey, ax)
        if self.env_q20.default_blade_flag == 0:
            acc_phy_lib_elias.plot_msi_blade(self.twiss_no_mkp_q20, 'msi_test_1', 'msi_test_4', ax, dx=self.msi_blade_dx)
        else:
            acc_phy_lib_elias.plot_default_msi_blade(self.twiss_no_mkp_q20, ax, 'msi_test_1')
        acc_phy_lib_elias.plot_aper_real(self.new_pos_x_q20[self.ind_q20], self.aper_neat_x_q20[self.ind_q20], ax, offset=self.offset_x_q20[self.ind_q20])
        plt.text(31.5, 0.03, '$A_{{x, min}}= {:.2f}$ \nfor dumped beam at MKP'.format(self.env_q20.A_x_mkp_dumped_beam), fontsize=15, horizontalalignment='left')
        plt.text(31.5, -0.09, 'n = {}'.format(self.env_q20.n), fontsize=15, horizontalalignment='left')

        
    #Function to plot Q26 optics beam envelope for dumped beam
    def plot_injection_q26_no_mkp(self, fig):
        fig.suptitle('SPS injection - Q26, dumped beam',fontsize=20)
        ax = fig.add_subplot(1, 1, 1)  # create an axes object in the figure
        ax.yaxis.label.set_size(20)
        ax.xaxis.label.set_size(20)
        plt.xticks(fontsize=14)  
        plt.yticks(fontsize=14)
        plt.ylim(-0.1, 0.15)
        plt.xlim(30, 92)
        acc_phy_lib_elias.plot_envelope(self.twiss_no_mkp_q26, self.env_q26.sige, self.env_q26.ex, self.env_q26.ey, ax)
        if self.env_q26.default_blade_flag == 0:
            acc_phy_lib_elias.plot_msi_blade(self.twiss_no_mkp_q26, 'msi_test_1', 'msi_test_4', ax, dx=self.msi_blade_dx)
        else:
            acc_phy_lib_elias.plot_default_msi_blade(self.twiss_no_mkp_q26, ax, 'msi_test_1')
        acc_phy_lib_elias.plot_aper_real(self.new_pos_x_q26[self.ind_q26], self.aper_neat_x_q26[self.ind_q26], ax, offset=self.offset_x_q26[self.ind_q26])
        plt.text(31.5, 0.03, '$A_{{x, min}}= {:.2f}$ \nfor dumped beam at MKP'.format(self.env_q26.A_x_mkp_dumped_beam), fontsize=15, horizontalalignment='left')
        plt.text(31.5, -0.09, 'n = {}'.format(self.env_q26.n), fontsize=15, horizontalalignment='left')
        

        #Function to plot SFTPRO optics beam envelope for dumped beam
    def plot_injection_sftpro_no_mkp(self, fig):
        fig.suptitle('SPS injection - SFTPRO, dumped beam',fontsize=20)
        ax = fig.add_subplot(1, 1, 1)  # create an axes object in the figure
        ax.yaxis.label.set_size(20)
        ax.xaxis.label.set_size(20)
        plt.xticks(fontsize=14)  
        plt.yticks(fontsize=14)
        plt.ylim(-0.1, 0.15)
        plt.xlim(30, 92)
        acc_phy_lib_elias.plot_envelope(self.twiss_no_mkp_sftpro, self.env_sftpro.sige, self.env_sftpro.ex, self.env_sftpro.ey, ax, nx=6, ny=4)
        if self.env_sftpro.default_blade_flag == 0:
            acc_phy_lib_elias.plot_msi_blade(self.twiss_no_mkp_sftpro, 'msi_test_1', 'msi_test_4', ax, dx=self.msi_blade_dx)
        else:
            acc_phy_lib_elias.plot_default_msi_blade(self.twiss_no_mkp_sftpro, ax, 'msi_test_1')
        acc_phy_lib_elias.plot_aper_real(self.new_pos_x_sftpro[self.ind_sftpro], self.aper_neat_x_sftpro[self.ind_sftpro], ax, offset=self.offset_x_sftpro[self.ind_sftpro])
        plt.text(31.5, 0.03, '$A_{{x, min}}= {:.2f}$ \nfor dumped beam at MKP'.format(self.env_sftpro.A_x_mkp_dumped_beam), fontsize=15, horizontalalignment='left')
        plt.text(31.5, -0.09, 'n = {}'.format(self.env_sftpro.n), fontsize=15, horizontalalignment='left')
    