"""
Collection of functions for accelerator physics, 
in particular to facilitate the SPS injection repository
Elias Waagaard - last updated 20/01/2022
"""

import numpy as np
import pandas as pd
import scipy as sp
import matplotlib.pyplot as plt
import matplotlib as mpl
from scipy.interpolate import interp1d


# ------------------------------------------ PRACTICAL FUNCTIONS FOR SEQUENCES ------------------------------------------------------------------

#Function to print elements in sequence 
def print_seq(madx):
    #Print the elements in the reduced short sequence
    dash = '-' * 65
    print('{:<27} {:>12} {:>15} {:>8}'.format("Element", "Location", "Type", "Length"))
    print(dash)
    for ele in madx.sequence['short_injection_basic'].elements:
        print('{:<27} {:>12.6f} {:>15} {:>8.3}'.format(ele.name, ele.at, ele.base_type.name, ele.length))
        
    return

#If context manager has been used, print the lines of the temporary error file 
def print_madx_error():
    with open('tempfile', 'r') as f:
        lines = f.readlines()
        for ele in lines:
            if '+=+=+= fatal' in ele:
                print('{}'.format(ele))

# ------------------------------------------ FUNCTIONS RELATED TO TWISS PARAMETER AND ENVELOPE PLOTTING -----------------------------------------

#Function to plot optics functions (beta, D) in both planes, with magnets on top
def plot_twiss(fig, twiss):
    gs = mpl.gridspec.GridSpec(3, 1, height_ratios=[1, 4, 4])
    ax1 = fig.add_subplot(gs[0])
    ax2 = fig.add_subplot(gs[1], sharex=ax1)
    ax3 = fig.add_subplot(gs[2], sharex=ax1)

    plt.setp(ax1.get_xticklabels(), visible=False)
    plt.setp(ax2.get_xticklabels(), visible=False)

    # top plot is synoptic
    ax1.axis('off')
    ax1.set_ylim(-1.2, 1)
    ax1.plot([0, twiss['s'].max()], [0, 0], 'k-')

    for _, row in twiss[twiss['keyword'].str.contains('quadrupole|rbend|sbend')].iterrows():
        if row['keyword'] == 'quadrupole':
            _ = ax1.add_patch(
                mpl.patches.Rectangle(
                    (row['s']-row['l'], 0), row['l'], np.sign(row['k1l']),
                    facecolor='k', edgecolor='k'))
        elif (row['keyword'] == 'rbend' or 
              row['keyword'] == 'sbend'):
            _ = ax1.add_patch(
                mpl.patches.Rectangle(
                    (row['s']-row['l'], -1), row['l'], 2,
                    facecolor='None', edgecolor='k'))

    #2nd plot is beta functions
    ax2.set_ylabel(r'$\beta$ (m)')
    ax2.plot(twiss['s'], twiss['betx'], 'r-',label=r'$\beta_x$')
    ax2.plot(twiss['s'], twiss['bety'], 'b-',label=r'$\beta_y$')    
    ax2.legend()

    #3rd plot is dispersion functions
    ax3.set_ylabel('D (m)')
    ax3.plot(twiss['s'], twiss['dx'], 'r-',label=r'$D_x$', linestyle = 'dotted')
    ax3.plot(twiss['s'], twiss['dy'], 'b-',label=r'$D_y$', linestyle = 'dotted')
    ax3.legend()

    axnames = ax1.twiny()
    axnames.spines['top'].set_visible(False)
    axnames.spines['left'].set_visible(False)
    axnames.spines['right'].set_visible(False)
    ax1._shared_x_axes.join(ax1, axnames)

    ticks, ticks_labels = list(), list()
    for keyword in ['quadrupole', 'rbend', 'sbend']:
        sub_twiss = twiss[twiss['keyword'] == keyword]
        ticks += list(sub_twiss['s'])
        ticks_labels += list(sub_twiss.index)

    axnames.set_xticks(ticks)
    axnames.set_xticklabels(ticks_labels, rotation=90)

    ax3.set_xlabel('s (m)')

    fig.tight_layout()
    fig.subplots_adjust(hspace=0)
    ax1.set_xlim(twiss['s'].min(), twiss['s'].max())

    
#Function to plot beam envelope with aperture, can choose horizontal (default) or vertical (hv==2)  
def plot_envelope(twiss, sige, ex, ey, ax, hv=None, nx=5, ny=5):
    if ax is None:
        ax = plt.gca()

    #Define some parameters for the beam
    one_sigma_x = np.sqrt(ex*twiss.betx + (sige*twiss.dx)**2) #beam size in x
    one_sigma_y = np.sqrt(ey*twiss.bety + (sige*twiss.dy)**2) #beam size in y

    #Choose whether to plot horizontal or vertical axis
    if hv is None:
        ax.plot(twiss.s, twiss.x, color = "b")
        ax.set_ylabel("x [m]")
        ax.set_xlabel("s [m]")
        ax.fill_between(twiss.s, twiss.x+nx*one_sigma_x,twiss.x-nx*one_sigma_x, alpha = 0.4, color = "b")
    elif hv==2:
        ax.plot(twiss.s, twiss.y, color = "r")
        ax.set_ylabel("y [m]")
        ax.set_xlabel("s [m]")
        ax.fill_between(twiss.s, twiss.y+ny*one_sigma_y,twiss.y-ny*one_sigma_y, alpha = 0.4, color = "r")        
    else:
        print("Unvalid vertical parameter!")



# ----------------------------------------------------------- FUNCTIONS RELATED TO SEPTA BLADES (MSI) -------------------------------------------------------------

#Function to get default MSI blade coordinates, using today's first MSI unless a new MSI blade is used  
def get_default_msi_blade(twiss, first_msi='msi.118350'):
    
    #Extract MSI coordinates
    s_msi = np.array([12.35450000, 12.35450000, 7.85450000, 7.85450000, 12.35450000, 7.55450000, 7.55450000, 3.05450000, 3.05450000, 7.55450000]) 
    x_msi = np.array([0.060, 0.100, 0.140, 0.100, 0.060, 0.100, 0.140, 0.240, 0.200, 0.100])

    #Shift the values to the correct starting point and plot
    s_msi -= min(s_msi)
    s_msi += twiss.s[first_msi] - 1.05   #First of the MSI magnets, minus its half-length
    
    return s_msi, x_msi 

#Plot default MSI values from given data
def plot_default_msi_blade(twiss, ax, first_msi='msi.118350'):
    if ax is None:
        ax = plt.gca()
        ax.set_ylabel("x [m]")
        ax.set_xlabel("s [m]")
    
    #Extract MSI blade coordinates
    s_msi, x_msi = get_default_msi_blade(twiss, first_msi)

    ax.plot(s_msi, x_msi, color = "k")


#Plot default MSI values from given data, but reversed 
def plot_default_msi_blade_reversed(twiss, ax, first_msi='msi.118350'):
    if ax is None:
        ax = plt.gca()
        ax.set_ylabel("x [m]")
        ax.set_xlabel("s [m]")
    
    #Extract MSI blade coordinates
    s_msi, x_msi = get_default_msi_blade(twiss, first_msi)
    
    #Now flip the s coordinates around the msi.118350:
    for i in range(s_msi.size):
        s_msi[i] = s_msi[i] - 2*(s_msi[i] - twiss.s['msi.118350'])
    
    ax.plot(s_msi, x_msi, color = "k")


#Function to plot custom MSI blade of given thickness, starting from the first MSI. Designed for 4 MSI of hkicker type.  
def plot_msi_blade(twiss, first_msi, last_msi, ax, dx=0.04):
    if ax is None:
        ax = plt.gca()
        ax.set_ylabel("x [m]")
        ax.set_xlabel("s [m]")
            
    s, x = get_msi_blade(twiss, first_msi, last_msi, dx)
    
    ax.plot(s, x, color="k")

#Function to get coordinates for custom MSI blade of given thickness, given the first and last MSI positions 
def get_msi_blade(twiss, first_msi, last_msi, dx=0.04):
   
    #Find difference between first and last MSI magnet 
    delta_l = (twiss.loc[last_msi].s + twiss.loc[last_msi].l/2) - (twiss.loc[first_msi].s - twiss.loc[first_msi].l/2)
    s = np.array([delta_l+1.3, delta_l+1.3, delta_l/2+0.8, delta_l/2+0.8, delta_l+1.3, delta_l/2+0.5, delta_l/2+0.5, 0.0 , 0.0 , delta_l/2+0.5])
    x = np.array([0.100-dx, 0.100, 0.100+dx, 0.100, 0.100-dx, 0.100, 0.100+0.04, 0.200+0.04, 0.200, 0.100])  #thickness of last half of MSI is chosen, first half is always 0.04
    s += twiss.loc[first_msi].s-twiss.loc[first_msi].l/2  #add the starting point of the first MSI

    return s, x

# ---------------------------------------------- FUNCTIONS RELATED TO ACCEPTANCE -------------------------------------------------------------------

#Function to print the smallest acceptance from Twiss
def print_smallest_acceptance(twiss, aper_x, aper_y, ex, ey):
    
    #Find the acceptance value of the vertical axis
    A_x = np.divide(aper_x - abs(twiss['x']), np.sqrt(twiss['betx']*ex))
    A_y = np.divide(aper_y - abs(twiss['y']), np.sqrt(twiss['bety']*ey))

    #Find the minimum aperture value
    val_x, idx_x = min((val_x, idx_x) for (idx_x, val_x) in enumerate(A_x))
    val_y, idx_y = min((val_y, idx_y) for (idx_y, val_y) in enumerate(A_y))

    print("Minimum value of acceptance A_x = {:.8f}, at element {}, s = {:.4f} m".format(val_x, twiss.iloc[idx_x].name, twiss.iloc[idx_x].s))
    print("Minimum value of acceptance A_y = {:.8f}, at element {}, s = {:.4f} m".format(val_y, twiss.iloc[idx_y].name, twiss.iloc[idx_y].s))
    
    
#Function to extract smallest value of acceptance, only considering one point per element (default setting from MADX)
def get_smallest_acceptance(twiss, aper_x, aper_y, ex, ey):
    
    #Find the acceptance value of the vertical axis
    A_x = np.divide(aper_x - abs(twiss['x']), np.sqrt(twiss['betx']*ex))
    A_y = np.divide(aper_y - abs(twiss['y']), np.sqrt(twiss['bety']*ey))

    #Find the minimum aperture value
    val_x, idx_x = min((val_x, idx_x) for (idx_x, val_x) in enumerate(A_x))
    val_y, idx_y = min((val_y, idx_y) for (idx_y, val_y) in enumerate(A_y))
    
    return A_x, A_y, val_x, idx_x, val_y, idx_y


#Function to extract smallest value of acceptance, with aperture values from the get_real_aperture function, where 
#points are evaluated both at start and end of each element through extrapolation 
#The missing points from Twiss commands are replaced with splines of these values - also possible to add aperture offsets
def get_smallest_acceptance_real(twiss, aper_x, aper_y, new_pos_x, new_pos_y, aper_index, ex, ey, offset_x=None, offset_y=None):
   
    #Define interpolation (extrapolate if any csv-read value lies slightly outside)
    f_betx = interp1d(twiss.s, twiss.betx,  fill_value="extrapolate")    #kind='cubic', 
    f_bety = interp1d(twiss.s, twiss.bety, fill_value="extrapolate")
    f_x = interp1d(twiss.s, twiss.x, fill_value="extrapolate")
    f_y = interp1d(twiss.s, twiss.y, fill_value="extrapolate")
    
    #Make sure aperture has the right sign 
    sign_x = np.sign(f_x(new_pos_x[aper_index]))
    sign_y = np.sign(f_y(new_pos_y[aper_index]))
    aper_x_sign = np.multiply(aper_x[aper_index], sign_x)
    aper_y_sign = np.multiply(aper_y[aper_index], sign_y)
    
    #Find the acceptance value of the vertical axis, add aperture offset if desired
    if offset_x is not None:
        A_x = np.divide(abs(aper_x_sign + offset_x[aper_index]) - abs(f_x(new_pos_x[aper_index])), np.sqrt(f_betx(new_pos_x[aper_index])*ex))
    else:
        A_x = np.divide(aper_x[aper_index] - abs(f_x(new_pos_x[aper_index])), np.sqrt(f_betx(new_pos_x[aper_index])*ex))
    
    if offset_y is not None:    
        A_y = np.divide(abs(aper_y_sign + offset_y[aper_index]) - abs(f_y(new_pos_y[aper_index])), np.sqrt(f_bety(new_pos_y[aper_index])*ey))
    else:
        A_y = np.divide(aper_y[aper_index] - abs(f_y(new_pos_y[aper_index])), np.sqrt(f_bety(new_pos_y[aper_index])*ey))

    #Find the position of the non-nan values 
    real_pos_x = new_pos_x[aper_index][np.isnan(A_x) == False]
    real_pos_y = new_pos_x[aper_index][np.isnan(A_x) == False]

    #Remove the nan-values elements
    A_x = [x for x in A_x if np.isnan(x) == False]
    A_y = [x for x in A_y if np.isnan(x) == False]

    #Find the minimum aperture value
    val_x, idx_x = min((val_x, idx_x) for (idx_x, val_x) in enumerate(A_x))
    val_y, idx_y = min((val_y, idx_y) for (idx_y, val_y) in enumerate(A_y))
    
    # ----- uncomment if result printing is desired, not for optimisation ---------------------------  
    #print("Minimum value of acceptance A_x = {:.8f} at s = {:.4f} m".format(val_x, real_pos_x[idx_x]))
    #print("Minimum value of acceptance A_y = {:.8f} at s = {:.4f} m".format(val_y, real_pos_y[idx_y]))
    
    return A_x, A_y, val_x, idx_x, real_pos_x, val_y, idx_y, real_pos_y


#Function to extract smallest value of acceptance from MSI blade, using two points per elements
def get_smallest_acceptance_real_MSI(twiss, ex, first_msi, last_msi, dx, default_blade_flag=0):
   
    #Define linear interpolation for x and beta_x from Twiss (better fit than cubic for s_msi)
    f_betx = interp1d(twiss.s, twiss.betx)
    f_x = interp1d(twiss.s, twiss.x)
    
    #Choose whether to use customised MSI blade or today's default MSI blade 
    if default_blade_flag == 0:
        s, x = get_msi_blade(twiss, first_msi, last_msi, dx)
    else:
        s, x = get_default_msi_blade(twiss, first_msi)
    
    #Find MSI aperture points closest towards the beam
    aper_msi_s = np.array([s[7], s[6],  s[2], s[1]])
    aper_msi_x = np.array([x[7], x[6],  x[2], x[1]])
    s_msi = np.linspace(aper_msi_s[0], aper_msi_s[-1], num=100)  #higher resolution of s over MSI
    
    #Do linear interpolation between MSI blade points 
    f_msi_x = interp1d(aper_msi_s, aper_msi_x)
    
    #Find the acceptance value of the vertical axis
    A_x = np.divide(f_x(s_msi) - f_msi_x(s_msi), np.sqrt(f_betx(s_msi)*ex))

    #Find the minimum aperture value
    val_x, idx_x = min((val_x, idx_x) for (idx_x, val_x) in enumerate(A_x))
    s_min = s_msi[idx_x]
    
    return val_x, s_min


# ---------------------------------------------- FUNCTIONS RELATED TO APERTURES --------------------------------------------------------------------

#Plug in twiss['aper_1'] or twiss['aper_2'] from Twiss command
#Function good for plotting, but does not give actual apertures as we only have one point per element!
def get_apertures(aper_raw):
    new_aper = list(aper_raw)
    for i in range(len(new_aper) - 1, 0, -1):
        if new_aper[i] != 0:
            new_aper[-1] = new_aper[i]
            break

    for i in range(len(new_aper)):

        if new_aper[i] == 0:
            new_aper[i] = search_next(i, new_aper)

    return np.array(new_aper)


#Function to extract apertures with two points per element - more realistic than the function "get_apertures" 
#If hv==2, vertical aperture is chosen
def get_apertures_real(twiss, hv=None):
    pos = list(twiss['s'])
    #Choose whether to plot horizontal or vertical axis:
    if hv is None:
        aper = list(twiss['aper_1'])
    elif hv==2:
        aper = list(twiss['aper_2'])
    else:
        print("Unvalid vertical parameter!")
        
    #Initiate arrays for new aperture 
    new_aper = aper[:] 
    new_pos = pos[:]
    indices = []

    #Search for all non-zero aperture elements 
    for i in range(len(aper) - 1, 0, -1):
        if aper[i] != 0:
            new_aper.insert(i, aper[i])
            indices.append(i)

    indices = list(reversed(indices))

    #Keep track of exact position in new array with counter 
    counter = 0
    for j in indices:
        new_pos.insert(j + counter, (pos[j] - twiss.l[j]))
        counter += 1
    
    #Replace all zeros with Nan
    for i in range(len(new_aper)):
        if new_aper[i] == 0:
            new_aper[i] = np.nan
    
    return np.array(new_pos), np.array(new_aper) 


#Function to get aperture off-set (at exit of element) from Twiss compatible values from the function "get_apertures_real" 
#with possibility to add list with offsets for entry point of element if different from the exit point --> make sure twiss and offset vectors have same length! 
def get_aper_offset_real(twiss, hv=None, start_offset=None):
    if hv is None:
        aper = list(twiss['aper_1'])
        offset = list(twiss['apoff_1'])
    elif hv==2:
        aper = list(twiss['aper_2'])
        offset = list(twiss['apoff_2'])
    else:
        print("Unvalid vertical parameter!")

    #Initiate arrays for new aperture 
    new_offset= offset[:] 

    #First check if there are different start offsets
    if start_offset is None:
        #Search for all non-zero aperture elements 
        for i in range(len(aper) - 1, 0, -1):
            if aper[i] != 0:
                new_offset.insert(i, offset[i])
    else: #fill in exit offsets, fill in start offset if different 
        for i in range(len(aper) - 1, 0, -1):
            if aper[i] != 0:
                if start_offset[i] == 0:
                    new_offset.insert(i, offset[i])
                else:
                    new_offset.insert(i, start_offset[i])

    return np.array(new_offset) 

#Return list without empty elements
def search_next(i, list):
    for j in range(i, len(list)):
        if list[j] != 0:
            return list[j]

#Plot apertures, but only with one point per element (default of MADX)
def plot_aper(s, aper, unit="m", ax=None):
    if ax is None:
        ax = plt.gca()
    if not unit == "mm":
        ax.fill_between(s, aper, 0.2, step="pre", color="black", alpha=0.4)
        ax.fill_between(s, -1 * aper, -0.2, step="pre", color="black", alpha=0.4)
        ax.plot(s, aper, "k", drawstyle="steps")
        ax.plot(s, -1 * aper, "k", drawstyle="steps")
    else:
        ax.fill_between(s, aper, 0.2 * 1e3, step="pre", color="black", alpha=0.4)
        ax.fill_between(
            s,
            -1 * aper,
            -0.2 * 1e3,
            step="pre",
            color="black",
            alpha=0.4,
        )
        ax.plot(s, aper, "k", drawstyle="steps")
        ax.plot(s, -1 * aper, "k", drawstyle="steps")


#Plot more realistic apertures with two points per element, with arrays containing nan values where aperture is zero-valued, 
#which gives nicer plotting - also possibility to add offset 
def plot_aper_real(s, aper, unit="m", ax=None, offset=None):
    
    aper_flipped = aper.copy()
    for i in range(len(aper_flipped)):
        if aper_flipped[i] != np.nan:
            aper_flipped[i] = -1 * aper_flipped[i] 
    
    if offset is not None:  #Add offset if given
        for i in range(len(aper)):
            aper[i] = aper[i] + + offset[i]
            aper_flipped[i] = aper_flipped[i] + offset[i]
        
    if ax is None:
        ax = plt.gca()
    if not unit == "mm":
        ax.fill_between(s, aper, 0.2, color="black", alpha=0.4)  #previously step="pre", but do not use if entry and exit aperture offset differs 
        ax.fill_between(s, aper_flipped, -0.2, color="black", alpha=0.4) #previously step="pre"
        ax.plot(s, aper, "k")   #previously drawstyle="steps", but do not use if entry and exit aperture offset differs
        ax.plot(s, aper_flipped, "k") #drawstyle="steps"
    else:
        ax.fill_between(s, aper, 0.2 * 1e3, color="black", alpha=0.4)   #previously step="pre"
        ax.fill_between(
            s,
            aper_flipped,
            -0.2 * 1e3,
            color="black",
            alpha=0.4,
        )  #previously step="pre"
        ax.plot(s, aper, "k")  #previously drawstyle="steps"
        ax.plot(s, aper_flipped, "k"    )   #previously drawstyle="steps"
        


# -------------------------------------------------------- LOADING SPS INJECTION SEQUENCES ----------------------------------------------------

#Function to load simplified short SPS sequence, depending on the optics (optics options: q20, q26, sftpro)
def load_seq(madx, optics):
    
    #Call the shortened SPS injection sequence file
    madx.call("initial_twisses_and_sequences/sps_short_injection_basic_{}.seq".format(optics))

    #Load Twiss and survey parameters from before 
    twiss_reversed = pd.read_csv("initial_twisses_and_sequences/sps_injection_twiss_reversed_{}.csv".format(optics))
    twiss_forward_sps = pd.read_csv("initial_twisses_and_sequences/sps_injection_twiss_forward_{}.csv".format(optics))
    survey_sps_forward = pd.read_csv("initial_twisses_and_sequences/survey_sps_forward_{}.csv".format(optics))
    twiss_beta1 = twiss_reversed.iloc[-1] #locate Twiss parameters at the start of the loaded reversed sequence - act as initial conditions for later
    
    #Call the old aperture files for SPS 
    madx.call("sps_aperture_data/aperturedb_1.dbx")
    madx.call("sps_aperture_data/aperturedb_2.dbx")
    madx.call("sps_aperture_data/aperturedb_3.dbx")

    #Activate the aperture for the Twiss flag to include it in Twiss command! 
    madx.input('select,flag=twiss,clear;')
    madx.input('SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;')
    
    #Return beam info
    madx.use(sequence='short_injection_basic')
    sige = madx.sequence['short_injection_basic'].beam['sige']  #relative energy spread
    ex = madx.sequence['short_injection_basic'].beam['ex']
    ey = madx.sequence['short_injection_basic'].beam['ey']
    
    print_seq(madx)

    return twiss_reversed, twiss_forward_sps, survey_sps_forward, twiss_beta1, sige, ex, ey

#Function to load simplified short SPS sequence stripped of all non-fixed elements and instruments, depending on the optics (q20, q26, sftpro)
def load_stripped_seq(madx, optics):
    
    #Call the shortened SPS injection sequence file
    madx.call("initial_twisses_and_sequences/sps_short_injection_basic_{}.seq".format(optics))

    #Load Twiss and survey parameters from before 
    twiss_reversed = pd.read_csv("initial_twisses_and_sequences/sps_injection_twiss_reversed_{}.csv".format(optics))
    twiss_forward_sps = pd.read_csv("initial_twisses_and_sequences/sps_injection_twiss_forward_{}.csv".format(optics))
    survey_sps_forward = pd.read_csv("initial_twisses_and_sequences/survey_sps_forward_{}.csv".format(optics))
    twiss_beta1 = twiss_reversed.iloc[-1] #locate Twiss parameters at the start of the loaded reversed sequence - act as initial conditions for later
    
    #Call the old aperture files for SPS 
    madx.call("sps_aperture_data/aperturedb_1.dbx")
    madx.call("sps_aperture_data/aperturedb_2.dbx")
    madx.call("sps_aperture_data/aperturedb_3.dbx")


    #Activate the aperture for the Twiss flag to include it in Twiss command! 
    madx.input('select,flag=twiss,clear;')
    madx.input('SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;')
    
    #Return beam info
    madx.use(sequence='short_injection_basic')
    sige = madx.sequence['short_injection_basic'].beam['sige']  #relative energy spread
    ex = madx.sequence['short_injection_basic'].beam['ex']
    ey = madx.sequence['short_injection_basic'].beam['ey']

    #Strip sequence of all free parameters 
    madx.command.seqedit(sequence='short_injection_basic')
    madx.command.flatten()
    madx.command.remove(element='mdca.103004')
    madx.command.remove(element='msi.118350')
    madx.command.remove(element='msi.118380')
    madx.command.remove(element='msi.118520')
    madx.command.remove(element='msi.118550')
    madx.command.remove(element='mdva.11904')
    madx.command.remove(element='mkpa.11931')
    madx.command.remove(element='mkpa.11936')
    madx.command.remove(element='mkpc.11952')
    madx.command.remove(element='mkp.11955')
    madx.command.remove(element='mdsh.11971') 
    madx.command.remove(element='bph.12008')
    madx.command.flatten()
    madx.command.endedit()


    return twiss_reversed, twiss_forward_sps, survey_sps_forward, twiss_beta1, sige, ex, ey